<?php
	include 'header.php';
?>

<form action="search.php" method="POST">
	<input type="text" name="search" placeholder="Search">
	<button type="submit" name="submit-search">Search</button>
</form>

<h1>Ogled oglasov</h1>
<h2>Vsi oglasi</h2>

<div class="article-container">
	<?php
		$sql = "SELECT * FROM posts";
		$result = mysqli_query($conn, $sql);
		$queryResults = mysqli_num_rows($result);

		if ($queryResults > 0) {
			while ($row = mysqli_fetch_assoc($result)) {
				echo "<div class='article-box'>
					<h3>".$row['naziv']."</h3>
					<p>".$row['opis']."</p>
					<p>".$row['komentar']."</p>
					<p>".$row['regija']."</p>
				</div>";
			}
		}

	?>
</div>

</body>
</html>
