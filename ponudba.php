<?php 
	# pocetna stranica nakon logiranja
	include_once 'include/dbh.inc.php';
?>

<!DOCTYPE html>
<html>
<head>
	<title>Dodaj oglas</title>
	<link rel="stylesheet" type="text/css" href="dodaj-oglas.css">
	<link rel="stylesheet" type="text/css" href="style1.css">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
</head>
<body>

	<!-- navbar-->


  <nav class="navbar sticky-top navbar-expand-lg bg-dark">
    <div class="container">
      <a class="navbar-brand" href="#">MalaMalica</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
   <i class="fas fa-bars"></i>
  </button>

      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto w-100 justify-content-end">
          <li class="nav-item active">
            <a class="nav-link" href="#">Doma<span class="sr-only">(current)</span></a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">Profil</a>
            <li class="nav-item">
              <a class="nav-link" href="dodaj-oglas.php">Dodaj oglas</a>
              <li class="nav-item">
                <a class="nav-link" href="ponudba.php">Ponudba</a>
                <li class="nav-item">
                  <a class="nav-link" href="pregled.php">Pregled</a>
                </li>
        </ul>
      </div>
    </div>
  </nav>

  




	<!-- navbar-->


	<!-- Border -->

	
	
<div class="container-box">
	<form action="include/ponudba-add.inc.php" method="POST">
    <div class="row">
      <div class="col-25">
        <label for="fname">Naziv ponudbe</label>
      </div>
      <div class="col-75">
        <input type="text" name="naziv" placeholder="Naziv ponudbe">
      </div>
    </div>
    <div class="row">
      <div class="col-25">
        <label for="lname">Opomemba</label>
      </div>
      <div class="col-75">
        <input type="text" name="opis" placeholder="Komentar">
      </div>
    </div>
    <div class="row">
      <div class="col-25">
        <label for="country">Regija</label>
      </div>
      <div class="col-75">
        <select name="regija">
          <option value="koper">Koper</option>
          <option value="ljubljana">Ljubljana</option>
          <option value="maribor">Maribor</option>
        </select>
      </div>
    </div>
    <div class="row">
      <div class="col-25">
        <label for="subject">Opis</label>
      </div>
      <div class="col-75">
        <textarea name="komentar" placeholder="Opis" style="height:200px"></textarea>
      </div>
    </div>
    <div class="row">
      <button type="submit" name="submit"> Add </button>
    </div>
    </form>
</div>
	
	

</body>
</html>
