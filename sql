CREATE TABLE posts (
	id int(11) NOT NULL PRIMARY KEY 		AUTO_INCREMENT,
    subject VARCHAR(128) NOT NULL,
    content VARCHAR(1000) NOT NULL,
    date datetime NOT NULL
); 

INSERT INTO posts (subject, content, date) VALUES ('This is the subject', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam sit amet ex non felis commodo congue. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris et nulla varius, eleifend velit ut, efficitur diam. Duis eu pulvinar mauris. Maecenas quis pulvinar enim. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nullam ut sollicitudin velit. Curabitur consectetur leo at dolor pellentesque, eu pellentesque mi cursus.', '2020-12-17 12:23:01');
