<?php
	include 'header.php';
?>

<!DOCTYPE html>
<html>
<head>
  <title>Dodaj oglas</title>
  <link rel="stylesheet" type="text/css" href="dodaj-oglas.css">
  <link rel="stylesheet" type="text/css" href="style1.css">
  <link rel="stylesheet" type="text/css" href="pregled.css">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
</head>
<body>

  <!-- navbar-->


  <nav class="navbar sticky-top navbar-expand-lg bg-dark">
    <div class="container">
      <a class="navbar-brand" href="#">MalaMalica</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
   <i class="fas fa-bars"></i>
  </button>

      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto w-100 justify-content-end">
          <li class="nav-item active">
            <a class="nav-link" href="#">Doma<span class="sr-only">(current)</span></a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">Profil</a>
            <li class="nav-item">
              <a class="nav-link" href="dodaj-oglas.php">Dodaj oglas</a>
              <li class="nav-item">
                <a class="nav-link" href="#">Ponudba</a>
                <li class="nav-item">
                  <a class="nav-link" href="pregled.php">Pregled</a>
                </li>

        </ul>
      </div>
    </div>
  </nav>

<h1>Iskani oglas</h1>

<div class="article-container">
<?php 	
	if (isset($_POST['submit-search'])) {
		$search = mysqli_real_escape_string($conn, $_POST['search']);
		$sql = "SELECT * FROM posts WHERE naziv LIKE '%$search%' OR opis LIKE '%$search%' OR komentar LIKE '%$search%' OR regija LIKE '%$search%'";
		$result = mysqli_query($conn, $sql);
		$queryResults = mysqli_num_rows($result);

		if ($queryResults > 0){
			while ($row = mysqli_fetch_assoc($result)){
				echo "<div>
					<h3>".$row['naziv']."</h3>
					<p>".$row['opis']."</p>
					<p>".$row['komentar']."</p>
					<p>".$row['regija']."</p>
				</div>";
			}
		}else {
			echo "Nima rezultata iskanja";
		}
	}

?>
</div>
